<?php

/**
 * This is the model class for table "{{company}}".
 *
 * The followings are the available columns in table '{{company}}':
 * @property string $id
 * @property string $email
 * @property string $password
 * @property integer $enabled
 * @property integer $city_id
 * @property integer $sphere_id
 * @property string $phone_number
 * @property string $name
 * @property string $description
 * @property string $adress
 * @property integer $visit_point
 * @property integer $min_visit_interval
 * @property integer $fb_like_point
 * @property string $website
 * @property string $facebook
 * @property string $logo
 *
 * The followings are the available model relations:
 * @property ClientPoints[] $clientPoints
 * @property City $city
 * @property Sphere $sphere
 * @property Reward[] $rewards
 */
class Company extends ModelBase
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{company}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, password, city_id, sphere_id, phone_number, name, adress', 'required'),
			array('enabled, city_id, sphere_id, visit_point, min_visit_interval, fb_like_point', 'numerical', 'integerOnly'=>true),
			array('email, phone_number', 'length', 'max'=>50),
			array('password', 'length', 'max'=>32),
			array('name', 'length', 'max'=>100),
			array('adress, facebook', 'length', 'max'=>255),
			array('website, logo', 'length', 'max'=>150),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, enabled, city_id, sphere_id, phone_number, name, description, adress, visit_point, min_visit_interval, fb_like_point, website, facebook, logo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clientPoints' => array(self::HAS_MANY, 'ClientPoints', 'company_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'sphere' => array(self::BELONGS_TO, 'Sphere', 'sphere_id'),
			'rewards' => array(self::HAS_MANY, 'Reward', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'enabled' => 'Enabled',
			'city_id' => 'City',
			'sphere_id' => 'Sphere',
			'phone_number' => 'Phone Number',
			'name' => 'Name',
			'description' => 'Description',
			'adress' => 'Adress',
			'visit_point' => 'Visit Point',
			'min_visit_interval' => 'Min Visit Interval',
			'fb_like_point' => 'Fb Like Point',
			'website' => 'Website',
			'facebook' => 'Facebook',
			'logo' => 'Logo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('enabled',$this->enabled);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('sphere_id',$this->sphere_id);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('adress',$this->adress,true);
		$criteria->compare('visit_point',$this->visit_point);
		$criteria->compare('min_visit_interval',$this->min_visit_interval);
		$criteria->compare('fb_like_point',$this->fb_like_point);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('logo',$this->logo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
