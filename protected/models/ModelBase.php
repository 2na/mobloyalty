<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelBase
 *
 * @author Нурсултан
 */
class ModelBase extends CActiveRecord
{
       public function getIterator()
        {
                $attributes = $this->getAttributes();
                $relations = array();
                foreach ($this->relations() as $key => $related)
                {
                        if ($this->hasRelated($key))
                        {
                                $relations[$key] = $this->$key;
                        }
                }
                $all = array_merge($attributes, $relations);
                return new CMapIterator($all);
        }
}
