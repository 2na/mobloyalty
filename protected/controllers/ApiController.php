<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiController
 *
 * @author Нурсултан
 */
class ApiController extends Controller
{

        /**
         * Default response format
         * either 'json' or 'xml'
         */
        private $format = 'json';

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array();
        }

        /**
         * Renders JSON data of all companies in database
         * by @param $_GET['model']
         */
        public function actionCompanies()
        {

                try
                {
                        $data = Company::model()->findAll();

                        $this->renderJson($data);
                } catch (CDbException $exc)
                {
                        $this->renderJson(new ApiResponse($exc->getMessage()));
                }
        }

        /**
         * Renders Detailed View for company
         * 
         * @param int $id
         */
        public function actionClientPoints($id)
        {
                try
                {
                        $data = ClientPoints::model()->with(array('company.rewards'))->findByPk($id);

                        if (empty($data))
                                $this->renderJson(new ApiResponse("No items were found for model!"));
                        else
                                $this->renderJson($data);
                } catch (CDbException $exc)
                {
                        $this->renderJson(new ApiResponse($exc));
                }
        }

        /**
         * Increses the points of user for visit
         * 
         * @param int $id
         */
        public function actionVisit($id)
        {
                $response = new ApiResponse();
                try
                {
                        $data = ClientPoints::model()->with(array('company.rewards'))->findByPk($id);
                        if (empty($data))
                        {
                                $this->renderJson(new ApiResponse("No items were found for model!"));
                                return;
                        } else
                        {
                                $timeAfterInterval = new DateTime($data->last_visit);
                                $timeAfterInterval->add(new DateInterval("PT" . $data->company->min_visit_interval . "H"));
                                if ($timeAfterInterval < new DateTime())
                                {
                                        $this->increasePoints($data, $response);
                                } else
                                {
                                        $this->renderJson(new ApiResponse("Not enough time has been left. Try again later"));
                                }
                        }
                } catch (CDbException $exc)
                {
                        $this->renderJson(new ApiResponse($exc));
                }
        }

        private function increasePoints($data, $response)
        {
                $data->points = $data->points + $data->company->visit_point;
                $data->last_visit = new CDbExpression('NOW()');
                if ($data->save() === true)
                {
                        $response->success = true;
                        $response->error = "Points have been added succesfully";
                        $this->renderJson($response);
                } else
                {
                        $this->renderJson($response);
                }
        }

        /**
         * Increses the points of user for visit
         * 
         * @param int $id
         */
        public function actionLike($id)
        {
                $response = new ApiResponse();
                try
                {
                        $data = ClientPoints::model()->with(array('company.rewards'))->findByPk($id);
                        if (empty($data))
                        {
                                $this->renderJson(new ApiResponse("No items were found for model!"));
                                return;
                        } else
                        {
                                $data->points = $data->points + $data->company->fb_like_point;
                                if ($data->save() === true)
                                {
                                        $response->success = true;
                                        $response->error = "Points have been added succesfully";
                                        $this->renderJson($response);
                                } else
                                {
                                        $this->renderJson($response);
                                }
                        }
                } catch (CDbException $exc)
                {
                        $this->renderJson(new ApiResponse($exc));
                }
        }

        /**
         * Increses the points of user for visit
         * 
         * @param int $id
         */
        public function actionTakeReward($id, $points)
        {
                $response = new ApiResponse();
                try
                {
                        $data = ClientPoints::model()->with(array('company.rewards'))->findByPk($id);
                        if (empty($data))
                        {
                                $this->renderJson(new ApiResponse("No items were found for model!"));
                                return;
                        } else
                        {
                                $data->points = $data->points - $points;
                                if ($data->points < 0)
                                {
                                        $this->renderJson(new ApiResponse("You have not enoguh points to take this reward!"));
                                        return;
                                }
                                if ($data->save() === true)
                                {
                                        $response->success = true;
                                        $response->error = "Your points has been taken for reward";
                                        $this->renderJson($response);
                                } else
                                {
                                        $this->renderJson($response);
                                }
                        }
                } catch (CDbException $exc)
                {
                        $this->renderJson(new ApiResponse($exc));
                }
        }

        /**
         * Renders JSON by give data object
         * 
         * @param Object $data
         */
        private function renderJson($data)
        {
                header('Content-type: application/' . $this->format);
                if (isset($data))
                        echo CJSON::encode($data);
                Yii::app()->end();
        }

}
