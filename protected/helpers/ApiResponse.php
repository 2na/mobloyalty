<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiError
 *
 * @author Нурсултан
 */
class ApiResponse
{
        public $success = false;
        public $error;
        
        public function __construct($error = 'Unexpected Error!')
        {
                $this->error = $error;
        }
}
