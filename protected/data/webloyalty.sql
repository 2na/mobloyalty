-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2014 at 11:45 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webloyalty`
--

-- --------------------------------------------------------

--
-- Table structure for table `yii_city`
--

CREATE TABLE IF NOT EXISTS `yii_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `yii_city`
--

INSERT INTO `yii_city` (`id`, `name`) VALUES
(1, 'Almaty'),
(2, 'Astana'),
(3, 'Karaganda'),
(4, 'Aktau');

-- --------------------------------------------------------

--
-- Table structure for table `yii_client`
--

CREATE TABLE IF NOT EXISTS `yii_client` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `yii_client`
--

INSERT INTO `yii_client` (`id`, `name`) VALUES
(1, 'Nurs'),
(2, 'Nurba'),
(3, 'Aza'),
(4, 'Vasya');

-- --------------------------------------------------------

--
-- Table structure for table `yii_client_points`
--

CREATE TABLE IF NOT EXISTS `yii_client_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `company_id` bigint(20) unsigned NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `last_visit` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`,`company_id`),
  UNIQUE KEY `client_id_2` (`client_id`,`company_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `yii_client_points`
--

INSERT INTO `yii_client_points` (`id`, `client_id`, `company_id`, `points`, `last_visit`) VALUES
(1, 1, 1, 26, '2014-05-10 02:20:13'),
(2, 1, 2, 13, '2014-05-10 03:00:59'),
(3, 1, 3, 6, '2014-05-10 03:08:44'),
(4, 2, 1, 22, '2014-05-10 23:22:59'),
(5, 2, 2, 33, '2014-05-10 23:22:59'),
(6, 2, 3, 100, '2014-05-10 23:22:59'),
(7, 3, 2, 24, '2014-05-10 23:22:59');

-- --------------------------------------------------------

--
-- Table structure for table `yii_company`
--

CREATE TABLE IF NOT EXISTS `yii_company` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'E-mail',
  `password` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Password',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - active и 0 - noactive',
  `city_id` int(11) NOT NULL COMMENT 'City',
  `sphere_id` int(11) NOT NULL COMMENT 'Scope of activity',
  `phone_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Phone number',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `adress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visit_point` int(11) NOT NULL DEFAULT '1',
  `min_visit_interval` int(11) NOT NULL DEFAULT '2',
  `fb_like_point` int(11) NOT NULL DEFAULT '1',
  `website` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `city_id` (`city_id`),
  KEY `sphere_id` (`sphere_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `yii_company`
--

INSERT INTO `yii_company` (`id`, `email`, `password`, `enabled`, `city_id`, `sphere_id`, `phone_number`, `name`, `description`, `adress`, `visit_point`, `min_visit_interval`, `fb_like_point`, `website`, `facebook`, `logo`) VALUES
(1, 'c1@example.com', '12345', 1, 1, 1, '77071234567', 'Soil and Oil', 'Работа с астрофологической кортьенктурой рынка', 'Жандосова, Розыбакиева 12', 2, 2, 3, 'example1.com', 'facebook.com/example1', 'no logo'),
(2, 'c2@example.com', '12345', 1, 1, 1, '77071232337', 'SugarLife', 'Парикмахерская для детей и взрослых', 'Алтынсарина, Кабулова 11', 3, 3, 1, 'example2.com', 'facebook.com/example2', 'no logo'),
(3, 'c3@example.com', '12345', 1, 1, 2, '77071125567', 'FuelMaster', 'Работа с профилатропической кортьенктурой рынка', 'Аспанова,Казбатова 33Б', 1, 2, 2, 'example3.com', 'facebook.com/example3', 'no logo'),
(4, 'c4@example.com', '12345', 1, 1, 2, '77071234567', 'Морозка', 'Работа с астрофологической кортьенктурой рынка', 'Жандосова, Розыбакиева 12', 2, 2, 3, 'example4.com', 'facebook.com/example4', 'no logo');

-- --------------------------------------------------------

--
-- Table structure for table `yii_profile`
--

CREATE TABLE IF NOT EXISTS `yii_profile` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'User ID',
  `firstname` varchar(50) NOT NULL DEFAULT '' COMMENT 'First Name',
  `lastname` varchar(50) NOT NULL DEFAULT '' COMMENT 'Last Name',
  `uimage` varchar(255) NOT NULL DEFAULT '' COMMENT 'User Photo',
  `about` text NOT NULL COMMENT 'User About',
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yii_profile`
--

INSERT INTO `yii_profile` (`user_id`, `firstname`, `lastname`, `uimage`, `about`) VALUES
(1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `yii_reward`
--

CREATE TABLE IF NOT EXISTS `yii_reward` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `condition` varchar(150) DEFAULT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `company_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `yii_reward`
--

INSERT INTO `yii_reward` (`id`, `name`, `condition`, `points`, `company_id`) VALUES
(1, 'Чашка бодрящего кофе', '', 15, 1),
(2, 'Клубничный сорбет для двоих', '', 22, 1),
(3, 'Скидка 10% на чек', 'До 19:00', 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `yii_sphere`
--

CREATE TABLE IF NOT EXISTS `yii_sphere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `yii_sphere`
--

INSERT INTO `yii_sphere` (`id`, `name`) VALUES
(1, 'Бары'),
(2, 'Кафе'),
(3, 'Бильярд');

-- --------------------------------------------------------

--
-- Table structure for table `yii_user`
--

CREATE TABLE IF NOT EXISTS `yii_user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'E-mail',
  `password` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Password',
  `salt` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Sucure Code',
  `role` enum('ADMIN','MODERATOR','USER') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USER' COMMENT 'Role',
  `time_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Time',
  `time_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Last Time',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - active и 0 - noactive',
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'IP address',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `yii_user`
--

INSERT INTO `yii_user` (`uid`, `email`, `password`, `salt`, `role`, `time_create`, `time_update`, `enabled`, `ip`) VALUES
(1, 'admin@admin.com', 'b8da40bf357e3de6cf0f9570c7cff2c0', '4ff0449a4a31e7.87066262', 'ADMIN', '2014-05-09 13:16:04', '2014-05-09 13:21:15', 1, '::1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `yii_client_points`
--
ALTER TABLE `yii_client_points`
  ADD CONSTRAINT `yii_client_points_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `yii_client` (`id`),
  ADD CONSTRAINT `yii_client_points_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `yii_company` (`id`);

--
-- Constraints for table `yii_company`
--
ALTER TABLE `yii_company`
  ADD CONSTRAINT `yii_company_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `yii_city` (`id`),
  ADD CONSTRAINT `yii_company_ibfk_2` FOREIGN KEY (`sphere_id`) REFERENCES `yii_sphere` (`id`);

--
-- Constraints for table `yii_profile`
--
ALTER TABLE `yii_profile`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `yii_user` (`uid`) ON DELETE CASCADE;

--
-- Constraints for table `yii_reward`
--
ALTER TABLE `yii_reward`
  ADD CONSTRAINT `yii_reward_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `yii_company` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
